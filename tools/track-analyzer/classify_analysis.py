import glob
import os

os.mkdir('results')
os.mkdir('results/maps')
os.mkdir('results/maps/field_export')
os.mkdir('results/trajs')

os.mkdir('results/trajs/concat')
subsets_traj = [os.path.basename(t) for t in glob.glob('data_dir/traj_analysis/*')]
subsets_maps = [os.path.basename(t) for t in glob.glob('data_dir/map_analysis/*')]
subsets = list(set (subsets_traj).union(subsets_maps))

trajs_csv = glob.glob('data_dir/traj_analysis/*/*.csv')
trajs_png = glob.glob('data_dir/traj_analysis/*/*.png') + glob.glob('data_dir/traj_analysis/*/*.svg') + glob.glob('data_dir/traj_analysis/*/*.tiff')
maps_tiff = glob.glob('data_dir/map_analysis/*/*.png') + glob.glob('data_dir/map_analysis/*/*.svg') + glob.glob('data_dir/map_analysis/*/*.tiff')
maps_txt = glob.glob('data_dir/map_analysis/*/field_export/*.txt')


for sub in subsets:
    for trcsv in trajs_csv:
        if sub in trcsv:
            name = os.path.basename(trcsv)
            if 'all_data' in name:
                os.rename(trcsv, "results/trajs/concat/{}_{}".format(sub, name))
            else:
                os.rename(trcsv, "results/trajs/{}_{}".format(sub, name))
    for trpng in trajs_png:
        if sub in trpng:
            name = os.path.basename(trpng)
            os.rename(trpng, "results/trajs/{}_{}".format(sub, name))
    for maptiff in maps_tiff:
        if sub in maptiff:
            name = os.path.basename(maptiff)
            os.rename(maptiff, "results/maps/{}_{}".format(sub, name))
    for maptxt in maps_txt:
        if sub in maptxt:
            name = os.path.basename(maptxt)
            os.rename(maptxt, "results/maps/field_export/{}_{}".format(sub, name))