import argparse
import re
import subprocess
import sys

def main(annotation, columns, other, pastml):
    with open(annotation) as inputf:
        first_line = inputf.readline()
    splitted = re.split('[\t,]', first_line.strip())
    columns = columns.split(',')
    namelist = []
    for col in columns:
        namelist.append(splitted[int(col)-1])


    command = '{} --data {} --columns {} {} '.format(pastml, annotation, " ".join(namelist), other)
    print(command)
    proc = subprocess.Popen( args=command, shell=True)
    returncode = proc.wait()
    sys.exit(returncode)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="""transpose first line to column""",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "-o", dest="other", help="""all options"""
    )
    parser.add_argument(
        "-c", dest="columns", help="""columns separated by quote"""
    )
    parser.add_argument(
        "-d", dest="data", help="""annotation file"""
    )
    parser.add_argument(
        "-p", dest="pastml", help="""path to pastml"""
    )
    options = parser.parse_args()
    main(options.data, options.columns, options.other, options.pastml)
