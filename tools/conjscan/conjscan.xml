<tool id="ConjScan" name="ConjScan" version="2.0+galaxy2">
  <description>: MacSyFinder-based detection of Conjugative elements using systems modelling and similarity search</description>
  <stdio>
	<exit_code range="0" level="warning" description="" />
	<exit_code range="1:" level="fatal" description="Error" />
  </stdio>
  <macros>
    <import>macros.xml</import>
  </macros>
  <edam_operations>
        <edam_operation>operation_3672</edam_operation>
	<edam_operation>operation_1777</edam_operation>
  </edam_operations>
  <edam_topics>
        <edam_topic>topic_3301</edam_topic>
	<edam_topic>topic_0797</edam_topic>
  </edam_topics>
  <expand macro="requirements" />
  <command>
    <![CDATA[ 
    apptainer run -B /pasteur $__root_dir__/../singularity/macsyfinder  --sequence-db $input --db-type $params.dataset_type
    @DATASETTYPE@
    --models-dir  ${system.fields.path_model} --models  ${system.fields.value} 
    --worker \${GALAXY_SLOTS}
    @HMMEROPTIONS@
     --out-dir output_dir
    && 
    tar cvzf $hmmer_archive output_dir/hmmer_results
 ]]>   
  </command>
  <inputs> 
    <param name="input" type="data" format="fasta" label="Genome to scan"/>
    <expand macro="dataset_type" />
    <param name="system" type="select" label="Conjugative element to detect" help="">
      <options from_data_table="conjscan_2">
        <column name="name" index="2"/>
        <column name="value" index="1"/>
        <column name="path_model" index="0"/>
      </options>
    </param>
    <expand macro="hmmer_options" />
  </inputs>
  <outputs>
    <data format="tar" name="hmmer_archive" label="hmmer results archive, $tool.name on ${on_string}"/>
    <data format="txt" name="rejected_candidates_txt" label="rejected candidate systems, $tool.name on ${on_string}" from_work_dir="output_dir/rejected_candidates.txt">
    	<filter>params['dataset_type'] in ('ordered_replicon', 'gembase')</filter>
    </data>
    <data format="tsv" name="rejected_candidates_tsv" label="rejected candidate systems, $tool.name on ${on_string}" from_work_dir="output_dir/rejected_candidates.tsv">
        <filter>params['dataset_type'] in ('ordered_replicon', 'gembase')</filter>
    </data>
    <data format="txt" name="all_systems_txt" label="possible candidate systems, $tool.name on ${on_string}" from_work_dir="output_dir/all_systems.txt"/>
    <data format="tabular" name="all_systems_tsv" label="possible candidate systems, $tool.name on ${on_string}" from_work_dir="output_dir/all_systems.tsv"/>
    <data format="tabular" name="all_best_solutions" label="other best solutions found by $tool.name on ${on_string}" from_work_dir="output_dir/all_best_solutions.tsv">
        <filter>params['dataset_type'] in ('ordered_replicon', 'gembase')</filter>
    </data>
    <data format="tabular" name="best_solution_loners" label="hits identified as loners on best solution, by $tool.name on ${on_string}" from_work_dir="output_dir/best_solution_loners.tsv">
        <filter>params['dataset_type'] in ('ordered_replicon', 'gembase')</filter>
    </data>
    <data format="tabular" name="best_solution" label="best solution found by $tool.name on ${on_string}" from_work_dir="output_dir/best_solution.tsv">
        <filter>params['dataset_type'] in ('ordered_replicon', 'gembase')</filter>
    </data>
    <data format="tabular" name="best_solution_summary" label="summary output,$tool.name on ${on_string}" from_work_dir="output_dir/best_solution_summary.tsv">
        <filter>params['dataset_type'] in ('ordered_replicon', 'gembase')</filter>
    </data>
    <data format="txt" name="uncomplete_systems" label="uncomplete systems, $tool.name on ${on_string}" from_work_dir="output_dir/uncomplete_systems.txt">
        <filter>params['dataset_type'] in ('unordered')</filter>
    </data>
  </outputs>
  <tests>
    <test>
      <param name="input" value="KLPN001.B.00030.P003.prt"/>
      <param name="system" value="CONJScan-2.0.1/chromosome all" />
      <output name="macsyview_json" file="results.macsyfinder.json"/>
    </test>
  </tests>
  <help>
    
**Requirements**: a multifasta file with the protein sequence to be analysed.
    
**Information on MacsyFinder**
Protein secretion systems are important machineries for the cells to interact with their environment.
In bacteria, these systems are involved in nutriment access, competition, defence, virulence ...
For instance they are found in plant and animal symbionts and pathogens.
    
**MacSyFinder** is a program to model and detect macromolecular systems, genetic pathways in protein datasets.
In prokaryotes, these systems have often evolutionarily conserved properties:
they are made of conserved components, and are encoded in compact loci (conserved genetic architecture).
The user models these systems with MacSyFinder to reflect these conserved features, and to allow their
efficient detection. The detection of a system is based on the presence of a given amount of mandatory
and accessory components (searched with Hmmer using the protein profiles) in the pre-defined genetic architecture.

For further informations, please visite the DOCUMENTATION_ for MacsyFinder.

.. _DOCUMENTATION: http://macsyfinder.readthedocs.org/en/latest/index.html

**CONJScan models**: Models for detection of conjugative, decayed conjugative and mobilisable elements

This set of MacSyFinder's models are dedicated to the detection of conjugative, decayed conjugative and mobilisable elements.

The conjugative T4SS that can be detected are:

- T4SS_typeG
- T4SS_typeF
- T4SS_typeC
- T4SS_typeB
- T4SS_typeI
- T4SS_typeT
- T4SS_typeFA
- T4SS_typeFATA

Mobilisable elements detected will be described as:

- MOB

Decayed conjugative T4SS will be described as:

- dCONJ_typeG
- dCONJ_typeF
- dCONJ_typeC
- dCONJ_typeB
- dCONJ_typeI
- dCONJ_typeT
- dCONJ_typeFA
- dCONJ_typeFATA

CONJScan now includes two sets of models. One is specific to the detection of conjugative, decayed conjugative and mobilisable elements in chromosomes and the second to the detection in plasmids. They are compatible with **MacSyFinder version 2**.



**Authors**: Charles Coluzzi, Jean Cury and Eduardo Rocha for the models, developed from previous work by Julien Guglielmini on conjugation and Bertrand Neron and Sophie Abby on MacSyFinder.

  </help>

<citations>
  <citation type="doi">doi:10.24072/pcjournal.250</citation>
  <citation type="doi">doi:10.1007/978-1-4939-9877-7_19</citation>
</citations>
</tool>
