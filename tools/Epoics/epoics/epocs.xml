<tool id="epocs" name="Epocs" version="0.1.0" python_template_version="3.5">
    <description> maximum likelihood inference of correlated evolution</description>
    <requirements>
      <requirement type="package">singularity</requirement>
    </requirements>
    <command detect_errors="exit_code"><![CDATA[
          mkdir outputprefix &&
          #if $epicoutput.use:
            ln -s $epicoutput.input_signif_tab outputprefix/$epicoutput.input_signif_tab.name &&
          #end if
          #if $input_epics_tab:
              ln -s $input_epics_tab outputprefix/$input_epics_tab.name &&
          #end if
          singularity run -B /pasteur --app epocs $__root_dir__/../singularity/epoics 
          #if $epicoutput.use:
            -R outputprefix/$epicoutput.input_signif_tab.name 
          #else:
            #if $epicoutput.setevent1.set:
              -1 $epicoutput.setevent1.first_event
            #end if
            #if $epicoutput.setevent2.set:
              -2 $epicoutput.setevent2.second_event
            #end if
          #end if
          -C $cooccurrences $eval -O outputprefix -S$scenario "$outgroup" $pastmltree
    ]]></command>
    <inputs>
      <param name="pastmltree" type="data" format="newick,nhx" label="Annotated phylogenetic tree" help="Input here your phylogenetic tree with events reconstructed on the branches. Use ‘Annotate pastml tree’ to annotate your input tree based on pastml reconstructions."/>
      <param name="input_epics_tab" type="data" format="tabular" optional="true" label="Epics output file (optional)" help="epics format"/>
      
      <conditional name="epicoutput">
        <param type="boolean" checked="false" truevalue="yes" falsevalue="no" name="use" label="Use an input events file?" help="If you want to evaluate only specific events, please choose whether you want an all-against-one (fill the first box) or only to evaluate a specific pair (fill the two boxes). Please input numbers, corresponding to the position of the traits in your tree."/>
        <when value="no">
          <conditional name="setevent1">
            <param type="boolean" checked="false" truevalue="yes" falsevalue="no" name="set" label="Set a first event id to be tested?"/>
            <when value="no"/>
            <when value="yes">
              <param argument="-1" type="integer" value="1" name="first_event" label="First event id"/>
            </when>
          </conditional>
          <conditional name="setevent2">
            <param type="boolean" checked="false" truevalue="yes" falsevalue="no" name="set" label="Set a second event id to be tested?"/>
            <when value="no"/>
            <when value="yes">
              <param argument="-2" type="integer" value="2" name="second_event" label="Second event id"/>
            </when>
          </conditional>
        </when>
        <when value="yes">
          <param argument="-R" name="input_signif_tab" type="data" format="tabular" label="Epics output signif file" help="epics format"/>
        </when>
      </conditional>
      <param type="select" name="scenario" label="Select a scenario">
        <option value="1" >mu (-S1)</option>
        <option value="i" >mu1, mu2 (H0) (-Si)</option>
        <option value="x" >mu, lambda -- modified rates are mu*lambda -- (-Sx)</option>
        <option value="u" >mu1, nu1, mu2 (-Su)</option>
        <option value="U" >mu1, mu2, nu2 (-SU)</option>
        <option value="X" >mu, lambda, kappa --mu*lambda*kappa-- (-SX)</option>
        <option value="r" >mu1, mu2, lambda (reciprocal induction: E1&lt;-&gt;E2) -- modified rates are mu[12]*lambda -- (-Sr)</option>
        <option value="a" >mu1, mu2, mu2* (one-way induction: E1-&gt;E2) (-Sa)</option>
        <option value="b" >mu1, mu1*, mu2 (one-way induction: E2-&gt;E1) (-Sb)</option>
        <option value="l" >mu1, mu1*, mu2, mu2* (induction both ways) (-Sl)</option>
        <option value="I" >mu1, mu2, nu1, nu2 (state dependance, no induction) (-SI)</option>
        <option value="R" >mu1, mu2, lambda, kappa (reciprocal induction: E1&lt;-&gt;E2, and state dependance) --mu[12]*lambda*kappa-- (-SR)</option>
        <option value="A" >mu1, mu2, mu2*, kappa1, kappa2 (one-way induction: E1-&gt;E2) (-SA)</option>
        <option value="B" >mu1, mu1*, mu2, kappa1, kappa2 (one-way induction: E2-&gt;E1) (-SB)</option>
        <option value="L" >mu1, mu1*, kappa1, mu2, mu2*, kappa2 (induction + state dependance) (-SL)</option>
        <option value="default" selected="true">mu1, mu1*, nu1, nu1*, mu2, mu2*, nu2, nu2* (-Sdefault)</option>
      </param>
      <param name="outgroup" type="text" label="Set outgroup" help="leave empty if no outgroup is present. Otherwise, the outgroup takes the form name1[:name2:name3:…]"/>
      <param argument="-C" type="integer" value="10" name="cooccurrences" label="Maximum number of co-occurrences to consider in the analysis." help="if the number of co-occurrences in the annotated tree exceeds the requested value, epocs will use an approximate likelihood search based on a restricted space. This is to ensure convergence in the likelihood search. The default is 10 co-occurrences."/>
      <param argument="-I" type="boolean" checked="false" truevalue="-I" falsevalue="" name="eval" label="Evaluate the Initial State (otherwise, assume &lt;0,0&gt;)?" help="If selected, epocs will explore all possible initial states in the likelihood calculations."/>   
    </inputs>
    <outputs>
      <data format="tabular" name="output_mat" label="outputprefix_mat_epocs_${scenario}.tab" from_work_dir="outputprefix/outputprefix_mat_epocs_*.tab"/>
    </outputs>
    <help><![CDATA[
**Information on method**

Minimal likelihood framework to infer correlated evolution between discrete traits placed on a phylogenetic tree. For more details on the method, we refer the user to the PUBLICATION_

.. _PUBLICATION: https://doi.org/10.1093/sysbio/syab092. 

**Information on tool**

For more details on the tool itself, we refer the user to the GITHUB_

.. _GITHUB: https://github.com/Maxime5G/EpoicsCoevol. 
    ]]></help>
    <citations>
    <citation type="doi">doi.org/10.1093/sysbio/syab092</citation>
  </citations>

</tool>
