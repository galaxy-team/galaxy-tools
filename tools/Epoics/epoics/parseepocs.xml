<tool id="parse_epocs" name="Epocs output parser" version="0.1.0" python_template_version="3.5">
    <description> Parsing the result from epocs</description>
    <requirements>
      <requirement type="set_environment">R_LIBS</requirement>
      <requirement type="package" version="4.1.0">R</requirement>
      <!--<requirement type="package" version="1.2.1">r-tidyverse</requirement>
        <requirement type="package">singularity</requirement>-->
    </requirements>
    <command detect_errors="exit_code"><![CDATA[
        mkdir outputprefix &&
        #for $epoc in $epocs_output:
          ln -s $epoc.input_epocs_tab outputprefix/$epoc.input_epocs_tab.name &&
        #end for
        Rscript '${__tool_directory__}/ParseEpocsFinal_WithLambda.R' outputprefix 
	#if $eventids:
	  $eventids
	#end if
    ]]></command>
    <inputs>
      <repeat name="epocs_output" title="Parsing the result from epocs" min="2" max="4">
        <param name="input_epocs_tab" type="data" format="tabular" label="Epocs output" />
      </repeat>
      <param name="eventids" optional="true" type="data" format="tabular" label="event names" help="one-column file with the event names in correct order, corresponding to events in the annotated phylogenetic tree."/>
    </inputs>
    <outputs>
      <data format="tabular" name="bestmodel_ouput" label="${tool.name} on ${on_string} Best Model file" from_work_dir="outputprefix/outputprefix_BestModel.tab"/>
      <data format="tabular" name="concatresult_ouput" label="${tool.name} on ${on_string} Concatenate Results file" from_work_dir="outputprefix/outputprefix_ConcatenatedResults.tab"/>
      <data format="tabular" name="lrt_ouput" label="${tool.name} on ${on_string} LRT file" from_work_dir="outputprefix/outputprefix_LRT.tab"/>
    </outputs>
    <help><![CDATA[
Rscript to retrieve the best model to explain the correlated evolution between any two pairs in the phylogenetic tree. For now, the script can only parse the results when using models i, a, b and l. The input requested is **at least** two files from epocs: one run with the H0 model (i) and one run with any of the three others. Up to four files can be submitted.

The script will compare any nested models using likelihood ratio test. Next, we decide which is the best model by comparing the LRT values. Note that our decision is only indicative, as we have not yet come up with a generalist decision criterion to determine the best model. Here, we aim to minimize the number of parameters needed to explain the correlated evolution. For this, richer models (i.e., with more parameters) need to have a 1.5x increase in likelihood compared to the previous best model.

Three files are exported after parsing:
- a file with all scores, likelihoods etc (standard output from epocs) for all pair of events;
- a file with all the likelihood ratio tests, with LRTs and pvalues;
- a file with the best model for each pair of events considered.
    ]]></help>
</tool>
