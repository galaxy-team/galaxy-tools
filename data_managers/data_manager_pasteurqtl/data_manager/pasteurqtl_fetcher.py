#!/usr/bin/env python

import requests
import json
import zipfile

import argparse
from pathlib import Path

# from urllib.request import Request, urlopen
import shutil


DATASET_VERSIONS_URL = "https://api.figshare.com/v2/articles/5404762/versions"


def get_dataset_detail(version="1"):
    with requests.get(f"{DATASET_VERSIONS_URL}/{version}") as resp:
        return json.loads(resp.content)


def download_dataset(url, dataset_name, workdir: Path):
    file_path = workdir / dataset_name
    resp = requests.get(url, stream=True)
    try:
        with file_path.open("wb") as dst:
            for data in resp.iter_content(chunk_size=1024 * 1024):
                dst.write(data)
        if zipfile.is_zipfile(file_path):
            fh = zipfile.ZipFile(file_path, "r")
            fh.extractall(workdir)
            file_path.unlink()
        else:
            return
    except IOError:
        print(
            f"ERROR: Could not download file from figshare!"
            f" url={url}, path={dataset_name}"
        )


def main(args):
    workdir = Path.absolute(Path.cwd())
    # workdir.mkdir()
    dataset_detail = get_dataset_detail(version=args.version)
    dataset_rawname = dataset_detail["files"][0]["name"]
    dataset_name = dataset_detail["files"][0]["name"].split("_")[0]
    dataset_download_url = dataset_detail["files"][0]["download_url"]
    dataset_version = str(dataset_detail["version"])
    data_table_entry = dict(
        value=f"{dataset_name}_{dataset_version}",
        dbkey="genotype_data",
        version=dataset_version,
        name=f"{dataset_name} version {dataset_version}",
        doi=dataset_detail["doi"],
        description=dataset_detail["description"].split("<div>")[0],
        optional_description=dataset_detail["description"],
        created_date=dataset_detail["created_date"],
        path=dataset_name,
    )
    with open(args.output) as fh:
        params = json.load(fh)

    target_directory_tax = Path(params["output_data"][0]["extra_files_path"])
    target_directory_tax.mkdir(parents=True, exist_ok=True)

    # download genotype dataset
    download_dataset(dataset_download_url, dataset_rawname, workdir)

    # move dir to final target dir
    shutil.move(workdir / dataset_name, target_directory_tax)
    data_manager_json = dict(data_tables=dict(pasteurqtl=data_table_entry))
    with open(args.output, "w") as fh:
        json.dump(data_manager_json, fh, sort_keys=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create data manager json.")
    parser.add_argument("--out", dest="output", action="store", help="JSON filename")
    parser.add_argument(
        "--version", dest="version", action="store", default="1", help="dataset version"
    )

    args = parser.parse_args()

    main(args)
