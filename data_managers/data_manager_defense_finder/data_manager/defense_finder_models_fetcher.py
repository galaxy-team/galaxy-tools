#!/usr/bin/env python

import argparse
import json

import subprocess
import sys
from pathlib import Path


def main(args):
    dataset_basename = "defense-finder-models"
    models_version = args.version
    output_file = args.output
    compat_version = args.compat_version
    dataset_value = f"{dataset_basename}_{models_version}"
    with open(output_file) as fh:
        params = json.load(fh)
    target_directory = Path(params["output_data"][0]["extra_files_path"])
    target_directory.mkdir(parents=True, exist_ok=True)
    # download models with macsydata
    args = [
        "defense-finder",
        "update",
        "--models-dir",
        dataset_value,
    ]

    proc = subprocess.Popen(args=args, shell=False, cwd=target_directory)
    return_code = proc.wait()
    if return_code:
        print("Error building index.", file=sys.stderr)
        sys.exit(return_code)

    data_table_entry = dict(
        value=dataset_value,
        dbkey=dataset_basename,
        version=models_version,
        name=f"version {models_version}",
        description="MacSyFinder models allowing for a systematic search of anti-phage systems",
        path=dataset_value,
        compatibility_version=compat_version,
    )

    data_manager_json = dict(data_tables={"defense-finder": data_table_entry})

    # move dir to final target dir
    # shutil.move(workdir / dataset_name, target_directory_tax)
    # data_manager_json = dict(data_tables=dict(pasteurqtl=data_table_entry))
    with open(output_file, "w") as fh:
        json.dump(data_manager_json, fh, sort_keys=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create data manager json.")
    parser.add_argument("--out", dest="output", action="store", help="JSON filename")
    parser.add_argument(
        "--version", dest="version", action="store", default="1", help="model version"
    )
    parser.add_argument(
        "--soft-compat-version",
        dest="compat_version",
        action="store",
        default="2.0.0",
        help="Which version of DefenseFinder is compatible with these models",
    )

    args = parser.parse_args()
    main(args)
